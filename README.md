![License MIT](https://img.shields.io/badge/license-MIT-blue.svg) [![](https://img.shields.io/docker/stars/jamotion/ansible-odoodev.svg)](https://hub.docker.com/r/jamotion/ansible-odoodev 'DockerHub') [![](https://img.shields.io/docker/pulls/jamotion/ansible-odoodev.svg)](https://hub.docker.com/r/jamotion/ansible-odoodev 'DockerHub')

Docker image for odoo development deployed with ansible scripts.

